#access the web camera
#perform motion detection via MotionDetection
#saves the output frames to the web browser via Flask web framework

from detector import MotionDetector
from imutils.video import VideoStream
from flask import *
import imutils
import cv2
import time
import threading 
import simpleaudio as sa

app = Flask(__name__)

filename = 'beep.wav'
wave_obj = sa.WaveObject.from_wave_file(filename)

ip = "0.0.0.0"
portVal = 8000

#access integrated cam
videoStream = VideoStream(src=0).start()
#allow camera sensor to warm up
time.sleep(2.0)

outputFrame = None 

def sound_alert():
	play_obj = wave_obj.play()
	play_obj.wait_done()

def detect_motion(minFrameCount):
	global videoStream, outputFrame

	motionDetector = MotionDetector(accumWeight=0.1)
	total = 0

	while True:
        #Resize the image to make the algorithm run faster
        #Convert to grayscale.
        #Reduce noise
		frame = videoStream.read()
		frame = imutils.resize(frame, width=400)
		grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		grayFrame = cv2.GaussianBlur(grayFrame, (7, 7), 0)

        #compute untill we have at least minFrameCount frames
        #once it is reached, start performing background subtraction
		if total > minFrameCount:
			motion = motionDetector.detect(grayFrame)

			if motion is not None:
				(thresh, (minX, minY, maxX, maxY)) = motion
				cv2.rectangle(frame, (minX, minY), (maxX, maxY),(0, 0, 255), 2)
				t = threading.Thread(target = sound_alert)
				t.start()
		
		motionDetector.update(grayFrame)
		total += 1

		outputFrame = frame.copy()

def generate():
	global outputFrame

	while True:
		if outputFrame is None:
			continue

        # encode the frame in JPEG format
		(flag, encodedImage) = cv2.imencode(".jpg", outputFrame)

        #ensure the corectitude of the encoding
		if not flag:
			continue

		yield(b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + 
			bytearray(encodedImage.tobytes()) + b'\r\n')

@app.route("/")
def index():
	return render_template("index.html") 
    #call the Flask render_template on our HTML file

@app.route("/video_feed")
def video_feed():
	return Response(generate(),
		mimetype = "multipart/x-mixed-replace; boundary=frame")

if __name__ == '__main__':
	t = threading.Thread(target=detect_motion, args=(
		32,))
	t.daemon = True
	t.start()

	app.run(host=ip, port=portVal, debug=True,
		threaded=True, use_reloader=False)

