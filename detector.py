import numpy as np
import imutils
import cv2

class MotionDetector:
	def __init__(self, accumWeight=0.5):
		self.accumWeight = accumWeight

		self.bg = None

	def update(self, baseFrame):
		if self.bg is None:
			#initialize the background 
			self.bg = baseFrame.copy().astype("float")
			return
		#update the background
		#accumWeight represents the proportion of the input frame (baseFrame)
		cv2.accumulateWeighted(baseFrame, self.bg, self.accumWeight)

	#apply motion detection
	def detect(self, baseFrame, tVal=25):
	
		deltaFrame = cv2.absdiff(self.bg.astype("uint8"), baseFrame)
		thresholdFrame = cv2.threshold(deltaFrame, tVal, 255, cv2.THRESH_BINARY)[1]

		thresholdFrame = cv2.erode(thresholdFrame, None, iterations=2)
		thresholdFrame = cv2.dilate(thresholdFrame, None, iterations=2)

		cnts = cv2.findContours(thresholdFrame.copy(), cv2.RETR_EXTERNAL,
			cv2.CHAIN_APPROX_SIMPLE)
		cnts = imutils.grab_contours(cnts)
		(minX, minY) = (np.inf, np.inf)
		(maxX, maxY) = (-np.inf, -np.inf)

		if len(cnts) == 0:
			return None

		for c in cnts:
			(x, y, w, h) = cv2.boundingRect(c)
			(minX, minY) = (min(minX, x), min(minY, y))
			(maxX, maxY) = (max(maxX, x + w), max(maxY, y + h))

		return (thresholdFrame, (minX, minY, maxX, maxY))
